package com.carecloud.planner.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Setter
@Getter
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class Duration {
    @Column(name="appointment_type_duration_hours")
    private Integer hours;
    @Column(name="appointment_type_duration_min")
    private Integer minutes;
    @Column(name="appointment_type_duration_sec")
    private Integer seconds;
}
