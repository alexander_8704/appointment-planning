package com.carecloud.planner.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.util.Date;

@Setter
@Getter
@Entity
@Table(name="appointment_types")
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentType {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="appointment_type_id")
    private Long id;
    @Column(name="appointment_type_name")
    private String name;
    @Embedded
    private Duration duration;
    @Column(name="appointment_type_color")
    private String color;
    @Column(name="appointment_created_on")
    @CreatedDate
    private Date creationDate;
}
