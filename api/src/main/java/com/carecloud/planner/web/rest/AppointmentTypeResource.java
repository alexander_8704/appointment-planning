package com.carecloud.planner.web.rest;

import com.carecloud.planner.entity.AppointmentType;
import com.carecloud.planner.repository.AppointmentTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController("/appointment-type")
public class AppointmentTypeResource {
    @Autowired
    private AppointmentTypeRepository appointmentTypeRepository;

    @PostMapping
    public void save(@RequestBody AppointmentType appointmentType){
        appointmentTypeRepository.save(appointmentType);
    }

    @GetMapping
    public List<AppointmentType> list(){
        return appointmentTypeRepository.findAll(new Sort(new Sort.Order(Sort.Direction.DESC, "creationDate")));
    }

    @PutMapping
    public void delete(@RequestBody AppointmentType appointmentType){
        appointmentTypeRepository.delete(appointmentType);
    }
}
