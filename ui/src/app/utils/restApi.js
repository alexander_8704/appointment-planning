import {SERVER_ADDRESS} from '../constants/global-constants';

export class RestAPI {
  /** @ngInject */
  constructor($resource) {
    this.$resource = $resource;
  }
  _resolveURL(resourceURL) {
    return SERVER_ADDRESS + resourceURL;
  }

  doPost(url, data, options = {}) {
    return new Promise((resolve, reject) => {
      this.$resource(this._resolveURL(url), {}, {post: Object.assign({method: 'POST'}, options)}).post(data)
        .$promise.then(serverData => {
          resolve(serverData);
        }, serverData => {
          reject(serverData);
        });
    });
  }

  doPut(url, data, options = {}) {
    return new Promise((resolve, reject) => {
      this.$resource(this._resolveURL(url), {}, {put: Object.assign({method: 'PUT'}, options)}).put(data)
        .$promise.then(serverData => {
          resolve(serverData);
        }, serverData => {
          reject(serverData);
        });
    });
  }

  doGet(url, params, options = {}) {
    return new Promise((resolve, reject) => {
      this.$resource(this._resolveURL(url), {}, {get: Object.assign({method: 'GET', isArray: true}, options)}).get(params)
        .$promise.then(serverData => {
          resolve(serverData);
        }, serverData => {
          reject(serverData);
        });
    });
  }
}
