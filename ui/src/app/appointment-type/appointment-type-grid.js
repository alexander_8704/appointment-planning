class AppointmentTypeGridController {
  handleSelection(selectedItem) {
    this.onSelection({selectedItem: angular.copy(selectedItem)});
  }

  handleDeleteRequest(selectedItem) {
    this.onDeleteRequest({selectedItem: angular.copy(selectedItem)});
  }
}

export const appointmentTypeGrid = {
  template: require('./appointment-type-grid.html'),
  controller: AppointmentTypeGridController,
  bindings: {
    appointmentTypes: '<',
    onSelection: '&',
    onDeleteRequest: '&'
  }
};

