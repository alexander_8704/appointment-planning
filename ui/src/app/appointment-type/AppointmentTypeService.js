import Endpoints from '../constants/global-constants';

export class AppointmentTypeService {
  /** @ngInject */
  constructor(RestAPI) {
    this.RestAPI = RestAPI;
  }

  save(appointmentType) {
    return this.RestAPI.doPost(Endpoints.APPOINTMENT_TYPE_RESOURCE, appointmentType);
  }

  delete(appointmentType) {
    return this.RestAPI.doPut(Endpoints.APPOINTMENT_TYPE_RESOURCE, appointmentType);
  }

  list() {
    return this.RestAPI.doGet(Endpoints.APPOINTMENT_TYPE_RESOURCE);
  }
}
