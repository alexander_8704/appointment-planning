class AppointmentTypeCrudController {
  /** @ngInject */
  constructor($scope, $log, $window, AppointmentTypeService) {
    this.scope = $scope;
    this.logger = $log;
    this.window = $window;
    this.appointmentTypeService = AppointmentTypeService;
    this.$onInit = this.onInit.bind(this);
  }

  onInit() {
    this.appointmentType = {duration: {}};
    this.appointmentTypeService.list()
      .then(this.setAppointmentTypes.bind(this))
      .catch(this.handleApiError.bind(this));
  }

  handleApiError(serverData) {
    this.logger(`Server Error: ${serverData}`);
  }

  setAppointmentTypes(appointmentTypes) {
    this.appointmentTypes = appointmentTypes;
  }

  isSaveButtonEnabled() {
    return this.appointmentType.name && this.appointmentType.color &&
      (this.appointmentType.duration.hours || this.appointmentType.duration.minutes || this.appointmentType.duration.seconds);
  }

  isUpdateButtonVisible() {
    return this.appointmentType.id;
  }

  save(isSave) {
    if (isSave) {
      delete this.appointmentType.id;
    }
    this.appointmentTypeService.save(this.appointmentType)
      .then(serverData => {
        this.window.alert(`Registro ${isSave ? 'Almacenado' : 'Actualizado'} exitosamente...!`);
        this.onInit(serverData);
      });
  }

  onGridItemSelected(selectedItem) {
    this.appointmentType = selectedItem;
  }

  onGridItemDeleteRequest(selectedItem) {
    if (this.window.confirm('Est\u00e1 seguro de eliminar el registro?')) {
      this.appointmentTypeService.delete(selectedItem)
        .then(this.onInit.bind(this));
    }
  }
}

export const appointmentTypeCrud = {
  template: require('./appointment-type-crud.html'),
  controller: AppointmentTypeCrudController,
  bindings: {
  }
};
