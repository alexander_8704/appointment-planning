import angular from 'angular';
import {AppointmentTypeService} from './AppointmentTypeService';

import {appointmentTypeCrud} from './appointment-type-crud';
import {appointmentTypeGrid} from './appointment-type-grid';
// import {appointmentTypes} from './appointment-types';

export const appointmentTypeModule = 'appointmentTypeModule';

angular
  .module(appointmentTypeModule, [])
  .component('appointmentTypeCrud', appointmentTypeCrud)
  .service('AppointmentTypeService', AppointmentTypeService)
  .component('appointmentTypeGrid', appointmentTypeGrid);
  // .component('appointmentTypes', appointmentTypes);
