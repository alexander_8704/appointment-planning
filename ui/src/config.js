export default appConfig;

/** @ngInject */
function appConfig($qProvider, $httpProvider) {
  $httpProvider.defaults.useXDomain = true;
  $qProvider.errorOnUnhandledRejections(false);
}
