import angular from 'angular';
import 'angular-ui-router';
import 'angular-resource';
import 'angular-animate';
import 'angular-loading-bar';

import {appointmentTypeModule} from './app/appointment-type/index';
import routesConfig from './routes';
import appConfig from './config';
import {main} from './app/main';
import {durationEditor} from './app/components/duration-editor';
import {RestAPI} from './app/utils/restApi';

import './index.less';

export const app = 'app';

angular
  .module(app, [appointmentTypeModule, 'ngAnimate', 'ui.router', 'ngResource', 'angular-loading-bar'])
  .config(routesConfig)
  .config(appConfig)
  .component('app', main)
  .component('durationEditor', durationEditor)
  .service('RestAPI', RestAPI);
